function WhereAmI()
{
	
	if (sessionStorage.getItem("Point") == "Aspiration")
	{
		document.getElementById("image2").style.left ="605px";
		document.getElementById("image2").style.top ="460px";
	}
	else if (sessionStorage.getItem("Point") == "Conference")
	{
		document.getElementById("image2").style.left ="850px";
		document.getElementById("image2").style.top ="665px";
	}
	else if (sessionStorage.getItem("Point") == "Dining")
	{
		document.getElementById("image2").style.left ="830px";
		document.getElementById("image2").style.top ="470px";
	}
	else if (sessionStorage.getItem("Point") == "CarPark")
	{
		document.getElementById("image2").style.left ="850px";
		document.getElementById("image2").style.top ="410px";
	}
	else if (sessionStorage.getItem("Point") == "MultiHall")
	{
		document.getElementById("image2").style.left ="1550px";
		document.getElementById("image2").style.top ="555px";
	}
	else if (sessionStorage.getItem("Point") == "Auditorium")
	{
		document.getElementById("image2").style.left ="1340px";
		document.getElementById("image2").style.top ="557px";
	}
	else if (sessionStorage.getItem("Point") == "Terrace")
	{
		document.getElementById("image2").style.left ="1130px";
		document.getElementById("image2").style.top ="596px";
	}
	else if (sessionStorage.getItem("Point") == "Gazebo")
	{
		document.getElementById("image2").style.left ="1000px";
		document.getElementById("image2").style.top ="505px";
	}
	else if (sessionStorage.getItem("Point") == "Lounge")
	{
		document.getElementById("image2").style.left ="226px";
		document.getElementById("image2").style.top ="630px";
	}
	else if (sessionStorage.getItem("Point") == "Merah")
	{
		document.getElementById("image2").style.left ="506px";
		document.getElementById("image2").style.top ="256px";
	}
	else if (sessionStorage.getItem("Point") == "Brani")
	{
		document.getElementById("image2").style.left ="380px";
		document.getElementById("image2").style.top ="493px";
	}
	else if (sessionStorage.getItem("Point") == "Tekong")
	{
		document.getElementById("image2").style.left ="350px";
		document.getElementById("image2").style.top ="436px";
	}
	else if (sessionStorage.getItem("Point") == "Pasir")
	{
		document.getElementById("image2").style.left ="763px";
		document.getElementById("image2").style.top ="256px";
	}
	else if (sessionStorage.getItem("Point") == "Reception")
	{
		document.getElementById("image2").style.left ="630px";
		document.getElementById("image2").style.top ="700px";
	}
	else
	{
		return
	}
	
	document.getElementById("image2").style.opacity ="1";
}

function SetBrani()
{
    sessionStorage.setItem("Destination", "Brani")
	GoDestination();
}
function SetDineHall()
{
    sessionStorage.setItem("Destination", "DineHall")
	GoDestination();
}
function GoDestination()
{
	location.href="https://172.24.201.241/CameraMode.html";
}
function GoBack()
{
	location.href = "https://172.24.201.241/reference/goback.html";
}

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}