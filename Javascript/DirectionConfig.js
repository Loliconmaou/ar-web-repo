// register our component
AFRAME.registerComponent('markerhandler',
{

    init: function()
    {
        const AMarker = document.querySelector("#A-marker");
        const BMarker = document.querySelector("#B-marker");
        const CMarker = document.querySelector("#C-marker");
        const DMarker = document.querySelector("#D-marker");
        const EMarker = document.querySelector("#E-marker");
		const ReturnButton = document.querySelector("#Btn");

        if (sessionStorage.getItem("Destination") != null)
        {
			ButtonActive();
            Changer();
        }
		if (AMarker)
		{
			AMarker.addEventListener('click', function(ev)
			{
				if(AMarker.object3D.visible == true && ev.detail.cursorEl)
				{
					if (sessionStorage.getItem("Destination") == null)
					{
						sessionStorage.setItem("Point", "Reception");
						PlaceSelector();
					}
				}
			})
		}
		if (BMarker)
		{
			BMarker.addEventListener('click', function(ev)
			{
				if(BMarker.object3D.visible == true && ev.detail.cursorEl)
				{
					if (sessionStorage.getItem("Destination") == null)
					{
						sessionStorage.setItem("Point", "Aspiration");
						PlaceSelector();
					}
				}
			})
		}
		if (DMarker)
		{
			DMarker.addEventListener('click', function(ev)
			{
				if(DMarker.object3D.visible == true && ev.detail.cursorEl)
				{
					if (sessionStorage.getItem("Destination") == null)
					{
						sessionStorage.setItem("Point", "Brani");
						PlaceSelector();
					}
				}
			})
		}
		if (EMarker)
		{
			EMarker.addEventListener('click', function(ev)
			{
				if(EMarker.object3D.visible == true && ev.detail.cursorEl)
				{
					if (sessionStorage.getItem("Destination") == null)
					{
						sessionStorage.setItem("Point", "Dining");
						PlaceSelector();
					}
				}
			})
		}
		
		ReturnButton.addEventListener('click', function(ev)
		{
			if (sessionStorage.getItem("Destination") != null)
			{
				ButtonDeactive();
				sessionStorage.setItem("Point", "NotAvailable");
				PlaceSelector();
			}
		})


        GoVideo();
    }

});

function PlaceSelector()
{
    location.href = "https://172.24.201.241/map.html";
}

function GoVideo()
{
    const FMarker = document.querySelector("#F-marker");

    if (FMarker)
    {
        FMarker.addEventListener('click', function(ev)
        {
            if (FMarker.object3D.visible == true && ev.detail.cursorEl)
            {
                location.href = "https://172.24.201.241/Asset/TestVideo.mp4";
            }
        });
    }
}

function Changer()
{
    const Recept = document.querySelector("#ReceptMarker");
    const Aspiration = document.querySelector("#AspirationMarker");
    const Brani = document.querySelector("#BraniMarker");
    const Dine = document.querySelector("#DineMarker");

    if (sessionStorage.getItem("Destination") == "Brani")
    {
        DirectLeft(Recept);
        DirectLeft(Aspiration);
        DirectLeft(Dine);
        ReachDestination(Brani);
    }
    else if (sessionStorage.getItem("Destination") == "DineHall")
    {
        DirectLeft(Recept);
        DirectRight(Aspiration);
        DirectRight(Brani);
        ReachDestination(Dine);
    }

}

function DirectLeft(Location)
{
    Location.setAttribute('material', 'src:#arrow1; side: double; transparent: true; opacity: 1');
    Location.setAttribute('geometry', 'primitive: plane');
    Location.setAttribute('rotation', '-90 0 0');
    Location.setAttribute('animation', 'property: position; dir: normal; dur: 500; loop: true; from: 1 0 0; to: -1 0 0');
}

function DirectRight(Location)
{
    Location.setAttribute('material', 'src:#arrow1; side: double; transparent: true; opacity: 1');
    Location.setAttribute('geometry', 'primitive: plane');
    Location.setAttribute('animation', 'property: position; dir: normal; dur: 500; loop: true; from: -1 0 0; to: 1 0 0');
    Location.setAttribute('rotation', '-90 0 180');
}

function ReachDestination(Location)
{
    Location.setAttribute('material', 'src:#arrived; side: double; transparent: true; opacity: 1');
    Location.setAttribute('geometry', 'primitive: plane');
    Location.setAttribute('rotation', '-90 0 0');
}

function ButtonActive()
{
	const ReturnButton = document.querySelector("#Btn");
	
	ReturnButton.setAttribute('material', 'src:#ChangeDes; transparent: true; opacity: 1');
}

function ButtonDeactive()
{
	const ReturnButton = document.querySelector("#Btn");
	
	ReturnButton.setAttribute('material', 'src:#ChangeDes; transparent: true; opacity: 0');
}